## Explorando Marte  

This is my solution to the Xerpa [Challenge](CHALLENGE.md). 

### Documentation

#### Python

Using Python version 3

##### Usage: 

You need Python >= 3.4 in order to run this project. Or you can use a docker container (instructions below).

For interactive mode, run the following command: 

```
python3 python/run.py
``` 

To provide a file with the input data, run this command:

```
python3 python/run.py -f sample.txt
``` 

Run tests: 

``` 
cd python && python3 -m unittest
```

###### Using Docker

You can use docker to run this project.

``` 
docker run -it --rm -v "$PWD"/python:/src -w /src python:3 python3 run.py
```  

To run tests: 

`docker run -it --rm -v "$PWD"/python:/src -w /src python:3 python3 -m unittest` 

