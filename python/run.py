#!/usr/bin/python3
# -*- coding: utf-8 -*-
# run.py - Mars Explorer

import argparse
from probe import Probe

def prompt(body):
  """ A custom input prompt """
  return input('\033[1m{}\033[0m'.format(body))

# Main function to run this project
def main():
  print("\033[91m\n Mars Explorer.. Starting a new mission! \n\033[0m")
  start()

def scanning(area, probe_name):
  coords = get_coordinates(name=probe_name)
  moves = get_moviments()
  probe = Probe(area, coords)
  finalPosition = [probe.go(m) for m in moves]
  return finalPosition[-1]

def start():
  """ Start soil survey """
  area = get_area()

  # Fisrt Space Probe Moves
  probeDog = scanning(area, '1st Robot')

  # Second Space Probe Moves
  probeDroid = scanning(area, '2nd')
  
  print(probeDog)
  print(probeDroid)

def get_area(area=''):
  area = list(map(int, area.split()))

  while len(area) != 2:
    _input = prompt("Enter the size of the area to be explored: \n")
    try:
      area = list(map(int, _input.split()))
      break
    except ValueError:
      print("Oops! That was no valid number. Try again...")
  
  return area

def get_coordinates(coordinates='', name='1st'):
  """ Get probe positions and direction """
  _coordinates = coordinates.split()

  if len(_coordinates) == 3:
    try:
      positions = list(map(int, _coordinates[0:2]))
      direction = _coordinates[2]

      if direction not in 'NESW':
        print("That was no valid direction. Try again...")
        _coordinates = []
      else:
        _coordinates = positions
        _coordinates.append(direction)
        return _coordinates
    except ValueError:
      print("Oops! That was no valid positions. Try again...")
      _coordinates = []
  
  while len(_coordinates) != 3:
    _input = prompt("Enter the position of the {}: \n".format(name))
    _coordinates = get_coordinates(_input, name)

  return _coordinates

def get_moviments(moves=None):
  """ Get probe moviments """
  _moves = moves

  if not _moves:
    _input = prompt("Enter the moviments or type Q to quit: \n")

    if _input == 'Q':
      print("!! Bye")
      exit()
    else:
      _moves = list(_input)

  return _moves

def dataInput(txt):
  try:
    with open(txt, "r") as data:
      area = get_area(data.readline())
      probe1_coords = get_coordinates(data.readline())
      probe1_moves = get_moviments(data.readline())
      probe2_coords = get_coordinates(data.readline())
      probe2_moves = get_moviments(data.readline())

    # Fisrt Space Probe Moves
    probe1 = Probe(area, probe1_coords)
    finalPosition1st = [probe1.go(m.strip()) for m in probe1_moves]

    # Second Space Probe Moves
    probe2 = Probe(area, probe2_coords)
    finalPosition2nd = [probe2.go(m.strip()) for m in probe2_moves]
    
    print(finalPosition1st[-1])
    print(finalPosition2nd[-1])

  except Exception as e:
    print(e)

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("-f", "--file", help='file to read')
  args = parser.parse_args()

  _file = args.file
  if(_file):
    dataInput(_file)
  else:
    main()
