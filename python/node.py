class Node:
  """ 
  Node 

  This nodes can be linked each other.
  Useful to create a cycle or looped sequences, like a cardinal point or station years

  """
  def __init__(self, point, left=None, right=None):
    self.point = point
    self.left = left
    self.right = right

  def toLeft(self):
    """ Get the neighbor on the left """
    return str(self.left)

  def toRight(self):
    """ Get the neighbor on the right """
    return str(self.right)

  def __str__(self):
    return str(self.point)
