import unittest

from probe import Probe
 
class TestProbe(unittest.TestCase):
  """
  Test the add function from the mymath library
  """
  probe = Probe([5, 5], [1, 2, 'N']) # instantiate the Probe Class

  def test_turn_left_when_direction_is_north(self):
    """ When going north, when turning to the left, the probe should be directed to the weast. """
    self.probe.turnLeft()
    self.assertEqual(self.probe.currentDirection, 'W')

  def test_turn_right_when_direction_is_north(self):
    """ When going north, when turning to the right, the probe should be directed to the east. """
    self.probe = Probe([5, 5], [1, 2, 'N']) # back to initial position
    self.probe.turnRight()
    self.assertEqual(self.probe.currentDirection, 'E')

  def test_movement_when_goes_to_north(self):
    """ When going north, probe should move to up """
    self.probe = Probe([5, 5], [1, 2, 'N']) # back to initial position
    self.probe.move()
    self.assertEqual(self.probe.y, 3)

  def test_movements(self):
    """ When going east, probe should return final position """
    self.probe = Probe([5, 5], [3, 3, 'E']) # back to initial position
    [self.probe.go(m) for m in 'MMRMMRMRRM']
    self.assertEqual(self.probe.x, 5)
    self.assertEqual(self.probe.y, 1)
    self.assertEqual(self.probe.currentDirection, 'E')
 
if __name__ == '__main__':
  unittest.main()