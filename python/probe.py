from node import Node

class Probe:
  """ 
  Space Probe

  Initialize a new space probe.

  :param array plateau: area size of plateau
  :param array iniitalCoordenates: initial position and direction of the probe
  """

  def __init__(self, plateau, initialCoordinates):
    if not isinstance(plateau, list) or len(plateau) != 2:
      raise ValueError("Invalid plateau parameters.")

    if not isinstance(initialCoordinates, list) or len(initialCoordinates) != 3:
      raise ValueError("Invalid coordinate parameters =/")

    self.plateau = plateau
    self.coordenates = initialCoordinates
    self.x, self.y, self.currentDirection = self.coordenates
    self.directions = self.buildAWindRose()
  
  def __str__(self):
    return str(self.point)

  def turnLeft(self):
    """ Turn left """
    self.currentDirection = str(self.directions[self.currentDirection].toLeft())

  def turnRight(self):
    """ Turn right """
    self.currentDirection = str(self.directions[self.currentDirection].toRight())

  def move(self):
    """ Moviment probe """
    if self.currentDirection == 'N': self.y += 1
    elif self.currentDirection == 'S': self.y -= 1 
    elif self.currentDirection == 'E': self.x += 1 
    elif self.currentDirection == 'W': self.x -= 1
    return self.x, self.y

  def go(self, command):
    """ 
      Move or spin the probe 
      :param string command: moviment that probe should do
    """
    if command == 'M':
      self.move()
    elif command == 'R':
      self.turnRight()
    elif command == 'L':
      self.turnLeft()
    else:
      print('Invalid command: {}. Passing'.format(command))

    return '{} {} {}'.format(self.x, self.y, self.currentDirection)

  def buildAWindRose(self):
    """ Builds a wind Rose using a linked list to be able to move back and forward """
    self.north = Node('N')
    self.east = Node('E', left=self.north)
    self.south = Node('S', left=self.east)
    self.west = Node('W', left=self.south, right=self.north)
    self.north.left = self.west
    self.north.right = self.east
    self.east.right = self.south
    self.south.right = self.west
    return { 'N': self.north, 'E': self.east, 'S': self.south, 'W': self.west }