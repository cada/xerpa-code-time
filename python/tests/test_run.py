import unittest

from run import *

class TestRun(unittest.TestCase):
    """
    Main script
    """

    def test_get_area(self):
      """ Get area receives a string and returns a array with two positions """
      area = get_area('5 4')
      self.assertEqual(len(area), 2)
      self.assertEqual(area[0], 5)
      self.assertEqual(area[1], 4)

    def test_get_coordinates(self):
      """ Get area receives a string and returns a array with three positions """
      coords = get_coordinates('1 5 N')
      self.assertEqual(len(coords), 3)
      self.assertEqual(coords[0], 1)
      self.assertEqual(coords[1], 5)
      self.assertEqual(coords[2], 'N')


if __name__ == '__main__':
  unittest.main()